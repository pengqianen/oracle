# 实验二：用户权限管理    
班级：软件工程-4   学号：202010414414  姓名：彭乾恩
## 实验目的  
掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。   

## 实验内容  
Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：
- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。
## 实验步骤  
### 1. 以system登录到pdborcl，创建角色con_res_role和用户sale：
![create_user](%E5%88%9B%E5%BB%BA%E7%94%A8%E6%88%B7.png)
SQL> CREATE ROLE con_res_role;

角色已创建。

SQL> GRANT connect,resource,CREATE VIEW TO con_res_role;

授权成功。

SQL> CREATE USER sale IDENTIFIED BY 123  DEFAULT  TABLESPACE  users TEMPORARY temp;
CREATE USER sale IDENTIFIED BY 123  DEFAULT  TABLESPACE  users TEMPORARY temp
                                                                         *
第 1 行出现错误:
ORA-01907: 需要 TABLESPACE 关键字


SQL> CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

用户已创建。

### 2. 授权和分配空间：
![alter](%E6%8E%88%E6%9D%83%E5%92%8C%E5%88%86%E9%85%8D.png)
SQL> ALTER USER sale defaukt TABLESPACE "USERS";
ALTER USER sale defaukt TABLESPACE "USERS"
                *
第 1 行出现错误:
ORA-00922: 选项缺失或无效


SQL> ALTER USER sale defaukt TABLESPACE "USERS"

SQL> ALTER USER sale default TABLESPACE "USERS";

用户已更改。

SQL> ALTER USER sale QUOTA 50M ON users;

用户已更改。

SQL> GRANT con_res_role TO sale;

授权成功。

### 3. 新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据：
![insert](%E5%88%9B%E5%BB%BA%E8%A1%A8%E5%92%8C%E8%A7%86%E5%9B%BE%E5%B9%B6%E6%8F%92%E5%85%A5%E6%95%B0%E6%8D%AE1.png)
![insert2](%E5%88%9B%E5%BB%BA%E8%A1%A8%E5%92%8C%E8%A7%86%E5%9B%BE%E5%B9%B6%E6%8F%92%E5%85%A5%E6%95%B0%E6%8D%AE2.png)
连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production
SQL> 
ROLE

CON_RES_ROLE
CONNECT
RESOURCE
SODA_APP

CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;

SELECT * FROM customers_view;
表已创建。

SQL> 
已创建 1 行。

SQL> 
已创建 1 行。

SQL> 
视图已创建。

SQL> 
NAME

zhang
wang


### 4. 将customers_view的SELECT对象权限授予hr用户：
GRANT SELECT ON customers_view TO hr;
SQL> 
授权成功。
### 5. session_privs，session_roles可以查看会话权限和角色：
![](%E5%88%9B%E5%BB%BA%E8%A1%A8%E5%92%8C%E8%A7%86%E5%9B%BE%E5%B9%B6%E6%8F%92%E5%85%A5%E6%95%B0%E6%8D%AE1.png)
SQL> SHOW USER;
USER 为 "SALE"
SELECT * FROM session_privs;
SELECT * FROM session_roles;

PRIVILEGE

CREATE SESSION
CREATE TABLE
CREATE CLUSTER
CREATE VIEW
CREATE SEQUENCE
CREATE PROCEDURE
CREATE TRIGGER
CREATE TYPE
CREATE OPERATOR
CREATE INDEXTYPE
SET CONTAINER

已选择 11 行。
### 6. 用户hr连接到pdborcl，查询sale授予它的视图customers_view：
![](%E6%9F%A5%E8%AF%A2%E8%A1%A8%E5%92%8C%E8%A7%86%E5%9B%BE.png)
SQL> SELECT * FROM sale.customers;
SELECT * FROM sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在


SQL> SELECT * FROM sale.customers_view;

NAME

zhang
wang

### 7. 概要文件设置,用户最多登录时最多只能错误3次: 
![](%E9%AA%8C%E8%AF%81%E6%A6%82%E8%A6%81%E6%96%87%E4%BB%B6%E8%AE%BE%E7%BD%AE.png)
SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;  

验证
---------------------
![忘记截图（三次密码失败）]()
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


请输入用户名:  SALE
输入口令: 
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


请输入用户名:  SALE
输入口令: 
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


SP2-0157: 在 3 次尝试之后无法连接到 ORACLE, 退出 SQL*Plus

### 8. 数据库和表空间占用分析和查看数据库的使用情况：
![](%E6%95%B0%E6%8D%AE%E5%BA%93%E5%8D%A0%E7%94%A8%E6%9F%A5%E7%9C%8B.png)

SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES


SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
  8   where  a.tablespace_name = b.tablespace_name;

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      370	  24	    346      93.51
UNDOTBS1			      100    39.3125	60.6875      60.69
USERS					5     3.9375	 1.0625      21.25
SYSTEM				      260     8.0625   251.9375       96.9

### 9. 实验结束删除用户和角色:
![](%E5%88%A0%E9%99%A4.png)
drop role con_res_role;
drop user sale cascade;

角色已删除。

SQL> 
用户已删除。

## 实验结果和分析
本次实验的结果是可以通过终端命令对数据库创建用户并授予权限，也可以通过sqlserver进行相同的操作。对于sale用户创建了表和视图并插入数据之后，只有把试图也授权才能查看，可以之授权只读，也可以授权共享读写权限。在实验结束后要删除实验角色，否则会占用一定的空间。
## 实验总结
通过本次实验我熟练掌握了在Oracle中创建本地角色和用户的方法，为用户分配角色以及授予角色权限的方法，了解Oracle中不同用户之间共享对象的方法，了解Oracle中控制用户对表空间的使用方法，包括限额设置等。对Oracle的权限体系理解还需要进一步加强。
有些细节问题需要更加注意，如用户密码的设置、权限授予时角色和对象名称的大小写等，对于大小写的细节需要时常提醒自己。
