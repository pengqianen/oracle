2020级软件工程4班 彭乾恩 202010414414

# 实验1：SQL语句的执行计划分析与优化指导

#实验目的



分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。
```

# 实验内容


对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，
通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，
看看该工具有没有给出优化建议。
```

# 查询语句


查询一
set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT','Sales')
GROUP BY d.department_name;

输出结果：

Predicate Information (identified by operation id):
---------------------------------------------------
   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
Note
-----
   - this is an adaptive plan


统计信息
-------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	 10  consistent gets
	  0  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

分析
从hr数据库的departments和employees表中查询 IT 部门和 Sales 部门的总人数和平均工资，按照部门名称进行分组（GROUP BY d.department_name;
输出结果，其中包括部门总人数,平均工资,并将结果中的列名分别命名为 "部门总人数" 和 "平均工资"。
```
查询二
```
set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name in ('IT','Sales');

输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")


统计信息
------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	  9  consistent gets
	  0  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

分析
```
该查询语句查询的表同上;在结果集中只包括名称为 'IT' 或 'Sales' 的部门;
输出结果集，其中包括部门总人数,平均工资,并将结果中的列名分别命名为 "部门总人数" 和 "平均工资"。与前一个查询语句不同的是,这个语句使用了 HAVING 关键字来筛选结果,而不是在 WHERE 子句中添加条件。HAVING 通常用于在分组后进行过滤,而 WHERE 用于在分组前对原始数据进行过滤。

```
优化代码
```
set autotrace on

SELECT d.department_name,count(e.job_id) as "部门总人数",avg(e.salary) as "平均工资"
FROM  hr.departments d,hr.employees e
WHERE e.department_id=d.department_id and d.department_id in 
(SELECT department_id from hr.departments WHERE department_name in ('IT','Sales')) 
group by d.department_name;
```
输出结果：
-----------------------------------------------------------
               2  CPU used by this session
               8  CPU used when call started
              18  DB time
              36  Requests to/from client
              37  SQL*Net roundtrips to/from client
             448  bytes received via SQL*Net from client
           70172  bytes sent via SQL*Net to client
               1  calls to get snapshot scn: kcmgss
               1  calls to kcmgcs
               1  cursor authentications
               1  execute count
              37  non-idle wait count
               1  opened cursors cumulative
               1  opened cursors current
               1  parse count (total)
               1  process last non-idle time
               1  sorts (memory)
            1804  sorts (rows)
              37  user calls
分析：原始的查询语句使用了部门名来作为条件进行查询，不过存在一个问题，就是如果部门名称出现了拼写错误或者大小写不同等问题，就可能导致查询结果不准确；为了解决这个问题，可以将查询条件改成部门 ID，因为部门 ID 是唯一且不可更改的标识符，这样查询结果就是准确的，也能提高查询效率。